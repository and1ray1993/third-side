const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');

module.exports = (env, argv) => {
	const isProd = argv.mode === 'production';
	const isDev = !isProd;

	console.log('isProd', isProd);
	console.log('isDev', isDev);

	const filename = (ext) =>
		isProd ? `[name].[contenthash].bundle.${ext}` : `[name].bundle.${ext}`;
	const plugins = () => {
		const base = [
			new HtmlWebpackPlugin(
				{
					filename: 'index.html',
					template: './index.html',
				},
			),
			new HtmlWebpackPlugin({
					filename: '404.html',
					template: './404.html',
				},
			),
			new HtmlWebpackPlugin({
					filename: 'development.html',
					template: './development.html',
				},
			),
			new HtmlWebpackPlugin({
					filename: 'reconstruction.html',
					template: './reconstruction.html',
				},
			),
			new CopyPlugin({
				patterns: [
					{
						from: path.resolve(__dirname, 'src', 'assets'),
						to: path.resolve(__dirname, 'dist/assets/'),
					},
				],
			}),
			new MiniCssExtractPlugin({
				filename: filename('css'),
			}),
			new CleanWebpackPlugin(),
		];

		if(isDev) {
			base.push(new ESLintPlugin());
		}

		return base;
	};

	return {
		target: 'web',
		context: path.resolve(__dirname, 'src'),
		entry: {
			main: './index.js',
		},
		output: {
			path: path.resolve(__dirname, 'dist'),
			filename: filename('js'),
		},
		resolve: {
			extensions: ['.js'],
			alias: {
				'@': path.resolve(__dirname, 'src'),
				'@core': path.resolve(__dirname, 'src', 'core'),
			},
		},
		devServer: {
			port: '5000',
			open: true,
			hot: true,
		},
		devtool: isDev ? 'source-map' : false,
		plugins: plugins(),
		module: {
			rules: [
				{
					test: /\.s[ac]ss$/i,
					exclude: /node_modules/,
					use: [
						MiniCssExtractPlugin.loader,
						'css-loader',
						'sass-loader',
					],
				},
				{
					test: /\.m?js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env'],
						},
					},
				},
				{
					test: /\.(png|jpg|gif|svg|ttf)$/i,
					use: [
						{
							loader: 'url-loader',
							options: {
								esModule: false,
							},
						},
					],
				},
			],
		},
	};
};
