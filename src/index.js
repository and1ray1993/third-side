import 'core-js/stable';
import 'regenerator-runtime/runtime';
import Inputmask from "inputmask";
import './styles/index.scss';

import { Dropdown } from 'bootstrap';

document.addEventListener('DOMContentLoaded', () => {
	const dropdownElementList = [].slice.call(document.querySelectorAll('.dropdown-toggle'));
	dropdownElementList.map((dropdownToggleEl) => new Dropdown(dropdownToggleEl));

	const btns = document.querySelectorAll('.js-active');
	const deactiveClass = 'd-none';
	const activeBtnClass = 'btn-primary';
	const deactiveBtnClass = 'btn-outline-primary';
	const customer = 'customer';
	const executor = 'executor';

	document.addEventListener('click', (e) => {
		const target = e.target;
		if(target.classList.contains('js-active')) {
			e.preventDefault();

			btns.forEach(item => {
				item.classList.remove(activeBtnClass);
				item.classList.add(deactiveBtnClass);
			});
			target.classList.add(activeBtnClass);

			const id = target.dataset.id;

			if(id === customer) {
				document.querySelectorAll(`[data-show="${id}"]`).forEach(item => {
					item.classList.remove(deactiveClass);
				});
				document.querySelectorAll(`[data-show="${executor}"]`).forEach(item => {
					item.classList.add(deactiveClass);
				});
			}

			if(id === executor) {
				document.querySelectorAll(`[data-show="${id}"]`).forEach(item => {
					item.classList.remove(deactiveClass);
				});
				document.querySelectorAll(`[data-show="${customer}"]`).forEach(item => {
					item.classList.add(deactiveClass);
				});
			}
		}

		if(target.classList.contains('js-scroll')) {
			e.preventDefault();
			if(target.tagName === 'A') {
				scrollTo(target.getAttribute('href').slice(1));
			} else {
				scrollTo(target.getAttribute('data-href').slice(1));
			}
		}
	});

	const phone = document.querySelector('[name="phone"]')
	const mail = document.querySelector('[name="mail"]')

	Inputmask({'mask': '+7 (999) 999-9999'}).mask(phone);
	Inputmask({ alias: "email"}).mask(mail);

	const form = document.getElementById('form');

	form.addEventListener('submit', (e) => {
		e.preventDefault();

		let formData = new FormData(form);

		form.querySelectorAll('.js-active').forEach(item => {
			if(item.classList.contains(activeBtnClass)) {
				formData.append('role', item.textContent);
			}
		});

		const msgElement = document.querySelector('.response__text');

		fetch('send.php', {
			method: 'POST',
			body: formData,
		})
			.then(res => res.text())
			.then(data => {
				if(data === 'success') {
					msgElement.textContent = 'Ваша заявка принята';
					return;
				}
				msgElement.textContent = 'Что то пошло не так, повторите попытку';
			})
			.catch(() => {
				msgElement.textContent = 'Что то пошло не так, повторите попытку';
			});
	});

});

function scrollTo(id) {
	const element = document.getElementById(id);
	const y = element.getBoundingClientRect().top + window.pageYOffset;
	window.scrollTo({ top: y, behavior: 'smooth' });
}


